from socket import *

print("\n" , gethostbyname(gethostname()))
serverSocket = socket(AF_INET , SOCK_STREAM)
serverSocket.bind((gethostname() , 12345))
serverSocket.listen(1)

while True:
	print("Ready to serve")
	connSocket , addr = serverSocket.accept()
	try:
		message = connSocket.recv(1024)
		print(message)
		connSocket.send("HTTP/1.1 200 OK\r\n\r\n")
	except IOError:
		connSocket.send("HTTP/1.1 404 Not Found\r\n")
		connSocket.close()

serverSocket.close()
